package com.springcore.ref;

public class A {
	private int x;
	private B obref;
	public A()
	{
		super();
	}
	
	public A(int x, B b) {
		super();
		this.x = x;
		this.obref = b;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public B getB() {
		return obref;
	}
	public void setB(B b) {
		this.obref = b;
	}
	@Override
	public String toString() {
		return "A [x=" + x + ", b=" + obref + "]";
	}
	
}
